package cc.nctu1210.sample.koala6x;

import java.util.Arrays;

/**
 * Created by nyotie on 10-May-17.
 */

public class VelocityEstimation {
    private float samplingTime;
    private float[] Velocity = {0.0F, 0.0F, 0.0F};
    private float[] accEarth = new float[3];


    // --------------------------------------------------------------------------- SETTER AND GETTER

    public float getSamplingTime() {
        return samplingTime;
    }

    public void setSamplingTime(float samplingTime) {
        this.samplingTime = samplingTime;
    }

    public float[] getGlobalAcc() {
        return accEarth;
    }

    public void resetVelocity(){
        Arrays.fill(Velocity, 0.0f);
    }

    public float[] getVelocity(){
        return this.Velocity;
    }

    // --------------------------------------------------------------------------- CALLER

    public VelocityEstimation(float _samplingTime){
        samplingTime = _samplingTime;
    }

    public float[] push(float[] acc, float[] OrientationQuat) {
        if (acc[0] == 0.0F && acc[1] == 0 && acc[2] == 0)
            return this.Velocity;

        float[] accmsq = convertGtoMSQ(acc);
        accEarth = transformToEarthRef(accmsq, OrientationQuat);
        accEarth = subtractGravity(accEarth);

        updateVelocity(acc);
        //reset velocity if no movement detected.


        return this.Velocity;
    }

    // --------------------------------------------------------------------------- FEATURE CALCULATIONS

    private float[] convertGtoMSQ(float[] acc) {
        float[] accms2 = new float[3];
        for (int i = 0; i < 3; i++)
            accms2[i] = acc[i] * 9.81F;

        return accms2;
    }

    private float[] transformToEarthRef(float[] acc, float[] OriQuat) {
        float[] tempQuat = new float[4];
        float[] AccelerationRet = new float[3];

        tempQuat[0] = 0.0F - acc[0] * OriQuat[1] - acc[1] * OriQuat[2] - acc[2] * OriQuat[3];
        tempQuat[1] = 0.0F + acc[0] * OriQuat[0] - acc[1] * OriQuat[3] - acc[2] * OriQuat[2];
        tempQuat[2] = 0.0F + acc[0] * OriQuat[3] - acc[1] * OriQuat[0] - acc[2] * OriQuat[1];
        tempQuat[3] = 0.0F - acc[0] * OriQuat[2] - acc[1] * OriQuat[1] - acc[2] * OriQuat[0];

        AccelerationRet[0] = OriQuat[0] * tempQuat[1] - OriQuat[1] * tempQuat[0] + OriQuat[2] * tempQuat[3] - OriQuat[3] * tempQuat[2];
        AccelerationRet[1] = OriQuat[0] * tempQuat[2] - OriQuat[1] * tempQuat[3] + OriQuat[2] * tempQuat[0] - OriQuat[3] * tempQuat[1];
        AccelerationRet[2] = OriQuat[0] * tempQuat[3] - OriQuat[1] * tempQuat[2] + OriQuat[2] * tempQuat[1] - OriQuat[3] * tempQuat[0];


        return AccelerationRet;
    }

    private float[] subtractGravity(float[] _accEarth) {
        _accEarth[2] += 9.81;

        return _accEarth;
    }

    private void updateVelocity(float[] acc) {
        for (int i = 0; i < 3; i++)
            Velocity[i] = Velocity[i] + acc[i] * getSamplingTime();
    }
}
