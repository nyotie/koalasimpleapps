package cc.nctu1210.sample.koala6x;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicBoolean;

import cc.nctu1210.api.koala6x.KoalaDevice;
import cc.nctu1210.api.koala6x.KoalaService;
import cc.nctu1210.api.koala6x.KoalaServiceManager;
import cc.nctu1210.api.koala6x.SensorEvent;
import cc.nctu1210.api.koala6x.SensorEventListener;
import cc.nctu1210.view.ModelObject;
import cc.nctu1210.view.ScanListAdapter;

public class MainActivity extends Activity implements SensorEventListener, AdapterView.OnItemClickListener {

    private static final int REQUEST_ENABLE_BT = 1;
    /********
     * for SDK version > 21
     **********/
    public static ArrayList<KoalaDevice> mDevices = new ArrayList<>();  // Manage the devices
    public static ArrayList<AtomicBoolean> mFlags = new ArrayList<>();
    private ArrayList<ModelObject> mObjects = new ArrayList<>();
    private ArrayList<String> mMacs = new ArrayList<>();
    private KoalaServiceManager mServiceManager;
    private BluetoothAdapter mBluetoothAdapter;

    /* flags */
    private boolean isConnected = false;
    private boolean isStart = false; // start accepting data transmission from KoalaDevice
    private boolean start_recording = false;
    private static boolean deviceConnectedStatus = false;
    private String flagValue = "S";
    private boolean displayNews = false;

    /********
     * for SDK version > 21
     **********/
    private BluetoothLeScanner mBLEScanner;
    private ScanCallback mScanCallback; //For Lollipop+
    private Button btScan, btStart;
    private Button btDbExport, btDbClear;
    private Button btTxExport, btTxRename;
    private Spinner spinner_devselector;
    private TextView headingText;
    private TextView newsText;
    private EditText customFileName;

    //UI for user interaction
    private AlertDialog dialog;
    ListView listScan;
    private ProgressDialog progressDialog;
    private ScanListAdapter mScanListAdapter;
    private String mac = "";

    /* Sensor and device */
    private static KoalaDevice Connected_device;
    private static String Connected_MacAddr = new String();
    private boolean mBooleanServiceCreated = false;
    private static float[] acc_val  = new float[3];
    private static float[] gyr_val = new float[3];
    private static float[] mag_val = new float[3];
    private static float rssiVal = 0;

    private int dataRate = 50;
    private Madgwick madgwickQuat = new Madgwick((float) 1/dataRate);
    private VelocityEstimation EstVelocity = new VelocityEstimation((float) 1/dataRate);

    /* Permissions Shits */
    public static final int REQUEST_FINE_LOCATION = 0x01 << 11;
    public static final int REQUEST_COARSE_LOCATION = 0x01 << 12;
    public static final int REQUEST_EXTERNAL_STORAGE = 0x01 << 13;

    public static final String[] PERMISSIONS = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
    };

    String wFileName;

    /* databases */
    ThisDbHelper mydb;

    private int countWindow = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btScan = (Button) findViewById(R.id.bt_scan);
        btScan.setOnClickListener(mConnectToKoala);
        btStart = (Button) findViewById(R.id.bt_start);
        btDbExport = (Button) findViewById(R.id.bt_dbExport);
        btDbClear = (Button) findViewById(R.id.bt_dbClear);
        btTxExport = (Button) findViewById(R.id.bt_txExport);
        btTxRename = (Button) findViewById(R.id.bt_txRename);
        spinner_devselector = (Spinner) findViewById(R.id.spinner2);
        spinner_devselector.bringToFront();

        headingText = (TextView) findViewById(R.id.txtHeading);
        newsText = (TextView) findViewById(R.id.txtNews);
        customFileName = (EditText) findViewById(R.id.customFileName);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!checkStoragePermissions(this)) requestStoragePermissions(this);
//            if (!checkCoarseLocationPermissions(this)) requestCoarseLocationPermissions(this);
            if (!checkFineLocationPermissions(this)) requestFineLocationPermissions(this);
        }

        initBLE();
        initKoala();

        // init db tool
        mydb = new ThisDbHelper(this);

//        btTxRename.setOnClickListener(txRename);

        radioGroupInit();
        buttonFirstSetter();
    }


    // --------------------------------------------------------------------------------------------- Permissions

    public boolean checkStoragePermissions(MainActivity activity) {
        // Check if we have write permission
        return ActivityCompat.checkSelfPermission(activity, PERMISSIONS[1]) == PackageManager.PERMISSION_GRANTED;
    }
    public static void requestStoragePermissions(MainActivity activity) {
        ActivityCompat.requestPermissions(activity,new String[]{PERMISSIONS[1]},REQUEST_EXTERNAL_STORAGE);
    }

    public boolean checkCoarseLocationPermissions(MainActivity activity) {
        // Check if we can access location in coarse mode
        return ActivityCompat.checkSelfPermission(activity, PERMISSIONS[2]) == PackageManager.PERMISSION_GRANTED;
    }
    public static void requestCoarseLocationPermissions(MainActivity activity) {
        ActivityCompat.requestPermissions(activity,new String[]{PERMISSIONS[2]}, REQUEST_COARSE_LOCATION);
    }

    public boolean checkFineLocationPermissions(MainActivity activity) {
        // Check if we can access location in fine mode
        return ActivityCompat.checkSelfPermission(activity, PERMISSIONS[3]) == PackageManager.PERMISSION_GRANTED;
    }
    public static void requestFineLocationPermissions(MainActivity activity) {
        ActivityCompat.requestPermissions(activity, new String[]{PERMISSIONS[3]}, REQUEST_FINE_LOCATION);
    }

    private void initBLE() {
        /* check whether the BLE's device or not */
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, "BLE not supported", Toast.LENGTH_SHORT).show();
            finish();
        }

        final BluetoothManager mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = mBluetoothManager.getAdapter();
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, "BLE not supported", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    private void initKoala() {
        mServiceManager = new KoalaServiceManager(MainActivity.this);
        mServiceManager.registerSensorEventListener(this, SensorEvent.TYPE_ACCELEROMETER, KoalaService.MOTION_WRITE_RATE_20, KoalaService.MOTION_ACCEL_SCALE_8G, KoalaService.MOTION_GYRO_SCALE_1000);
        mServiceManager.registerSensorEventListener(this, SensorEvent.TYPE_GYROSCOPE);
        mServiceManager.registerSensorEventListener(this, SensorEvent.TYPE_MAGNETOMETER);
    }

    private void buttonFirstSetter(){
        btScan.setEnabled(true);
        btStart.setEnabled(false);
        btDbExport.setEnabled(false);
        btTxExport.setEnabled(false);
        btTxRename.setEnabled(false);
        spinner_devselector.setEnabled(false);
    }

    private void buttonExportMode(boolean ExState){
        if(ExState){
            btDbExport.setEnabled(true);
            btTxExport.setEnabled(true);
        } else {
            btDbExport.setEnabled(false);
            btTxExport.setEnabled(false);
        }
    }

    // ----------------------------------------------------------------------------- Button Listener


    View.OnClickListener mConnectToKoala = new View.OnClickListener() {
        public void onClick(View v) {
        if (mBooleanServiceCreated && !isConnected) {
            showDialogListView();
            scanLeDevice(true);
        }
        }
    };

    // ----------------------------------------------------------------------------- Button events

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bt_start:
                isStart = !isStart;

                if(isStart){
                    btStart.setText("Pause");
                    headingText.setText(String.valueOf(countWindow));
                    buttonExportMode(false);

                    EstVelocity.resetVelocity();
                    countWindow = 0;

                    dbClear();
                } else {
                    btStart.setText("Start");
                    displayNews = !displayNews;
                    buttonExportMode(true);
                }

                break;
            case R.id.bt_dbExport:
                dbExport();
                break;
            case R.id.bt_dbClear:
                dbClear();
                break;
            case R.id.bt_txExport:
                txExport();
                break;
//            case R.id.bt_txRename:
//                txRename();
//                break;
            default:
                break;

        }
    }

    private void dbExport(){
//        if(mydb.numberOfRows() < 1){
//            Toast.makeText(getBaseContext(), "No data to export", Toast.LENGTH_SHORT).show();
//            return;
//        }

        try {
            File KoalaFolder = new File(Environment.getExternalStorageDirectory(), getResources().getString(R.string.app_name));
            if(!KoalaFolder.exists())
                KoalaFolder.mkdir();

//                    File sd = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS);
//                    Log.i("sqlite", "loc -> " + sd.toString());

            if (KoalaFolder.canWrite()) {
//                    if (sd.canWrite()) {
                String currentDBPath = "//data/data/" + getPackageName() + "/databases/koalaRssi.db";
                File currentDB = new File(currentDBPath);
                File backupDB = new File(KoalaFolder, "koalaRssi.db");
//                        File backupDB = new File(sd, "koalaRssi.db");

                if (currentDB.exists()) {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();

//                    Log.i("sqlite","file length:" + src.size());

                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
//                    Log.i("sqlite", "copy db success");
                    Toast.makeText(getBaseContext(), "copy db success!", Toast.LENGTH_SHORT).show();
                }
            }
        } catch (Exception e) {
            Log.e("sqlite export", e.toString());
        }
    }
    private void dbClear(){
        try {
            mydb.clearTable();
            wFileName = null;
//            Log.i("sqlite", "table truncated");
//            Toast.makeText(getBaseContext(), "table truncated!", Toast.LENGTH_SHORT).show();
            newsText.setText("table truncated!");
        } catch (Exception e) {
            Log.e("sqlite clear", e.toString());
        }
    }

    private void txExport() {
        /* -----------------------------------EXPORT RAW DATA ----------------------------------------------------- */

        //check if filename is empty --> generate
        if (!customFileName.getText().toString().trim().equals("")){
            wFileName = String.format("Raw_%s_%s.csv", customFileName.getText().toString(),
                    new SimpleDateFormat("ddMMMyyyy_HHmmss", Locale.ENGLISH).format(new Date(System.currentTimeMillis())));
        } else {
            wFileName = String.format("Raw_%s.csv", new SimpleDateFormat("ddMMMyyyy_HHmmss", Locale.ENGLISH).format(new Date(System.currentTimeMillis())));
        }

        //prepare exported dir
        File ExportDir = new File(Environment.getExternalStorageDirectory(), getResources().getString(R.string.app_name));
        if(!ExportDir.exists())
            ExportDir.mkdir();

        //prepare exported file
        File file = new File(ExportDir, wFileName);
        try{
            file.createNewFile();
            CSVWriter csvWrite = new CSVWriter(new FileWriter((file)));
            Log.d("data count", String.valueOf(mydb.numberOfRawDataRows()));
            Cursor curCSV = mydb.getAllRawData(); //query to export
            csvWrite.writeNext(curCSV.getColumnNames());
            while(curCSV.moveToNext())
            {
                //Which column to exprort from query
                String arrStr[] ={curCSV.getString(curCSV.getColumnIndex("MacAddress")),
                        curCSV.getString(curCSV.getColumnIndex("rssi")),
                        curCSV.getString(curCSV.getColumnIndex("accX")),
                        curCSV.getString(curCSV.getColumnIndex("accY")),
                        curCSV.getString(curCSV.getColumnIndex("accZ")),
                        curCSV.getString(curCSV.getColumnIndex("gyroX")),
                        curCSV.getString(curCSV.getColumnIndex("gyroY")),
                        curCSV.getString(curCSV.getColumnIndex("gyroZ")),
                        curCSV.getString(curCSV.getColumnIndex("quat0")),
                        curCSV.getString(curCSV.getColumnIndex("quat1")),
                        curCSV.getString(curCSV.getColumnIndex("quat2")),
                        curCSV.getString(curCSV.getColumnIndex("quat3")),
                        curCSV.getString(curCSV.getColumnIndex("veloX")),
                        curCSV.getString(curCSV.getColumnIndex("veloY")),
                        curCSV.getString(curCSV.getColumnIndex("veloZ")),
                        curCSV.getString(curCSV.getColumnIndex("flag")),
                        curCSV.getString(curCSV.getColumnIndex("recdate"))};
                csvWrite.writeNext(arrStr);
            }
//            Toast.makeText(getBaseContext(), "text file exported!", Toast.LENGTH_SHORT).show();
            newsText.setText("text file exported!");
            wFileName = null;
            csvWrite.close();
            curCSV.close();
        } catch (Exception sqlEx) {
            Log.e("export txt", sqlEx.getMessage(), sqlEx);
//            Toast.makeText(getBaseContext(), "failed to export csv!", Toast.LENGTH_SHORT).show();
            newsText.setText("failed to export csv!");
        }
    }

    private void radioGroupInit(){
        RadioGroup rg = (RadioGroup) findViewById(R.id.radioGroup);

        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.radioButton1:
                        flagValue = "S";
                        customFileName.setText("S");
                        break;
                    case R.id.radioButton2:
                        flagValue = "R";
                        customFileName.setText("R");
                        break;
                    case R.id.radioButton3:
                        flagValue = "B";
                        customFileName.setText("B");
                        break;
                    case R.id.radioButton4:
                        flagValue = "T";
                        customFileName.setText("T");
                        break;
                    case R.id.radioButton5:
                        flagValue = "O";
                        customFileName.setText("O");
                        break;
//                    case R.id.radioButton5:
//                        flagValue = "5";
//                        break;
//                    case R.id.radioButton6:
//                        flagValue = "6";
//                        break;
//                    case R.id.radioButton7:
//                        flagValue = "7";
//                        break;
//                    case R.id.radioButton8:
//                        flagValue = "8";
//                        break;
                }
            }
        });
    }

    // ----------------------------------------------------------------------------- BLE & Koala connector

    private void scanLeDevice(boolean scanFlag) {
        if (scanFlag)
            mBLEScanner.startScan(mScanCallback);
        else
            mBLEScanner.stopScan(mScanCallback);
    }

    private int findKoalaWithMac(String macAddr) {
        if (mDevices.size() == 0)
            return -1;
        for (int i = 0; i < mDevices.size(); i++) {
            KoalaDevice tmpDevice = mDevices.get(i);
            if (macAddr.matches(tmpDevice.getDevice().getAddress()))
                return i;
        }
        return -1;
    }

    private void loadSpinner() {
        mObjects.clear();
        mMacs.clear();
        for (int i = 0, size = mDevices.size(); i < size; i++) {
            KoalaDevice d = mDevices.get(i);
            ModelObject object = new ModelObject(d.getDevice().getName(), d.getDevice().getAddress(), String.valueOf(d.getRssi()));
            mObjects.add(object);
            mMacs.add(object.getAddress());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, mMacs);
        spinner_devselector.setAdapter(adapter);
    }

    private void showDialogListView(){
        listScan = new ListView(this);
        mScanListAdapter = new ScanListAdapter(this,mObjects);
        listScan.setAdapter(mScanListAdapter);
        listScan.setOnItemClickListener(this);
        AlertDialog.Builder builder= new AlertDialog.Builder(MainActivity.this, android.R.style.Theme_Material_Light_Dialog_Alert);
        builder.setTitle("Scanning");
        builder.setCancelable(true);
        builder.setPositiveButton("Cancel",null);
        builder.setView(listScan);
        dialog = builder.create();
        dialog.show();
    }

    private void DismissDialogListView(){
        start_recording = !start_recording;
        scanLeDevice(false);
        btStart.setEnabled(true);
        dialog.dismiss();
    }

    // ----------------------------------------------------------------------------- Override events

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_COARSE_LOCATION:
                if (grantResults.length > 0) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(getBaseContext(), "coarse location permission granted", Toast.LENGTH_SHORT).show();
                    } else {
                        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
                        builder.setTitle("Functionality limited");
                        builder.setMessage("Since location access has not been granted, this app will not be able to discover beacons when in the background.");
                        builder.setPositiveButton(android.R.string.ok, null);
                        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                            }

                        });
                        builder.show();
                    }
                } else {
                    Toast.makeText(getBaseContext(), "no permission granted!!", Toast.LENGTH_SHORT).show();
                }
                break;
            case REQUEST_EXTERNAL_STORAGE:
                if (grantResults.length > 0) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(getBaseContext(), "external storage permission granted", Toast.LENGTH_SHORT).show();
                    } else {
                        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
                        builder.setTitle("Functionality limited");
                        builder.setMessage("Since storage access has not been granted, this app will not be able to store any images.");
                        builder.setPositiveButton(android.R.string.ok, null);
                        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                            }

                        });
                        builder.show();
                    }
                } else {
                    Toast.makeText(getBaseContext(), "no permission granted!!", Toast.LENGTH_SHORT).show();
                }
                break;
            case REQUEST_FINE_LOCATION:
                if (grantResults.length > 0) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(getBaseContext(), "fine location permission granted", Toast.LENGTH_SHORT).show();
                    } else {
                        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
                        builder.setTitle("Functionality limited");
                        builder.setMessage("Since storage access has not been granted, this app will not be able to store any images.");
                        builder.setPositiveButton(android.R.string.ok, null);
                        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                            }

                        });
                        builder.show();
                    }
                } else {
                    Toast.makeText(getBaseContext(), "no permission granted!!", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void onRSSIChange(String addr, float rssi) {
        final int position = findKoalaWithMac(addr);
        if (position != -1) {
            rssiVal = rssi;
//            displayRSSI(position, rssi);
        }
    }

    @Override
    public void onKoalaServiceStatusChanged(boolean status) {
        mBooleanServiceCreated = status;
    }

    @Override
    public void onSensorChange(final SensorEvent e) {
        if (!isStart) return;

        final int eventType = e.type;
        switch (eventType) {
            case SensorEvent.TYPE_ACCELEROMETER:
//                acc_val = lowPass(e.values.clone(), acc_val);
                acc_val = e.values.clone();

                float[] quat_val = madgwickQuat.updateQuat(gyr_val, acc_val);
                float[] velo_val = EstVelocity.push(acc_val, quat_val);

                mydb.insertNewRowRaw(spinner_devselector.getSelectedItem().toString(), rssiVal, acc_val, gyr_val, mag_val, quat_val, velo_val, flagValue);

                // need to reset the velocity value for time to time. but how?

                break;
            case SensorEvent.TYPE_GYROSCOPE:
                gyr_val = e.values.clone();
                break;
            case SensorEvent.TYPE_MAGNETOMETER:
                mag_val = e.values.clone();
                break;
        }

        countWindow++;
        headingText.setText(String.valueOf(countWindow));

//        try {
//            if(countWindow+1 >= winSize) {
//                FeatureExtract FE = new FeatureExtract();
//                Object[] extractedAcceFeatures = FE.main(xAccel,yAccel,zAccel).toArray();
//                Object[] extractedGyroFeatures = FE.main(xGyro,yGyro,zGyro).toArray();
//                getFeatureExtractions(extractedAcceFeatures, extractedGyroFeatures, eulerOrientation.getQuaternion());
//
//                xAccel.clear();
//                yAccel.clear();
//                zAccel.clear();
//                xGyro.clear();
//                yGyro.clear();
//                zGyro.clear();
//                countWindow=0;
//            } else {
//                xAccel.add(acc_val[0]);
//                yAccel.add(acc_val[1]);
//                zAccel.add(acc_val[2]);
//                xGyro.add(gyr_val[0]);
//                yGyro.add(gyr_val[1]);
//                zGyro.add(gyr_val[2]);
//
//                countWindow+=1;
//            }
//
//            if(!warmingUp) {
//                if(countWindow2+1 >= winSize) {
//                    FeatureExtract FE = new FeatureExtract();
//                    Object[] extractedAcceFeatures = FE.main(xAccel2,yAccel2,zAccel2).toArray();
//                    Object[] extractedGyroFeatures = FE.main(xGyro2,yGyro2,zGyro2).toArray();
//                    getFeatureExtractions(extractedAcceFeatures, extractedGyroFeatures, eulerOrientation.getQuaternion());
//
//                    xAccel2.clear();
//                    yAccel2.clear();
//                    zAccel2.clear();
//                    xGyro2.clear();
//                    yGyro2.clear();
//                    zGyro2.clear();
//                    countWindow2=0;
//                } else {
//                    xAccel2.add(acc_val[0]);
//                    yAccel2.add(acc_val[1]);
//                    zAccel2.add(acc_val[2]);
//                    xGyro2.add(gyr_val[0]);
//                    yGyro2.add(gyr_val[1]);
//                    zGyro2.add(gyr_val[2]);
//
//                    countWindow2+=1;
//                }
//            } else {
//                if (countWindow+1>=winSlideSize) warmingUp = false;
//            }
//
//            headingText.setText(String.valueOf(countWindow) + " - " + String.valueOf(countWindow2));
//        } catch (Exception zz){
//            Log.e("feat_error", zz.toString());
//        }

// old method where window = datarate
//        try{
//            if(countWindow+1 % dataRate == 0 || countWindow+1 >= winSize){
//                if (overlapWindow) getFeatureExtractions();
//                else {
//                    if (countWindow + 1 >= winSize) getFeatureExtractions();
//                }
//
//                if(countWindow+1 >= winSize) countWindow = 0; else countWindow+=1;
//            } else {
//                countWindow+=1;
//            }
//
//        } catch (Exception zz){
//            Log.e("feature", zz.toString());
//        }

        if(!displayNews){
//            Toast.makeText(getBaseContext(), "connection establised!", Toast.LENGTH_SHORT).show();
            newsText.setText("connection establised!");
            displayNews = !displayNews;
        }
    }

    @Override
    public void onConnectionStatusChange(boolean status) {
        deviceConnectedStatus = status;
        scanLeDevice(!deviceConnectedStatus);
        if (status){//connected
            progressDialog.dismiss();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try{
            if (requestCode == REQUEST_ENABLE_BT) {
                if (resultCode == Activity.RESULT_CANCELED) {
                    //Bluetooth not enabled.
                    finish();
                    return;
                }
            }
            super.onActivityResult(requestCode, resultCode, data);
        } catch(Exception e) {

        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        // ========================================================================================= BLE
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, 1);
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                /* Lollipop +  */
                mBLEScanner = mBluetoothAdapter.getBluetoothLeScanner();
                ScanSettings settings = new ScanSettings.Builder()
                        .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                        .build();
                List<ScanFilter> filters = new ArrayList<ScanFilter>();

                mScanCallback = new ScanCallback() {
                    @Override
                    public void onScanResult(int callbackType, ScanResult result) {
                        final ScanResult scanResult = result;
                        final BluetoothDevice device = scanResult.getDevice();
                        new Thread() {
                            @Override
                            public void run() {
                                if (device != null) {
                                    if (!start_recording) {
                                        KoalaDevice p = new KoalaDevice(device, scanResult.getRssi(), scanResult.getScanRecord().getBytes());
                                        int position = findKoalaWithMac(device.getAddress());
                                        if (position == -1) {
                                            AtomicBoolean flag = new AtomicBoolean(false);
                                            mDevices.add(p);
                                            mFlags.add(flag);
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    // This code will always run on the UI thread, therefore is safe to modify UI elements.
                                                    loadSpinner();
                                                    mScanListAdapter.notifyDataSetChanged();
                                                }
                                            });
                                        }
                                    } else {
                                        //automatic connection
                                        if (!deviceConnectedStatus) {
                                            String address = device.getAddress();
                                            if (address.equals(mac)) {
                                                mServiceManager.connect(address);
                                            }
                                        }
                                    }
                                }
                            }
                        }.start();
                    }
                };
            } else {
                /* JellyBean/Kitkat  */
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        for (int i = 0; i < mFlags.size(); i++) {
            AtomicBoolean flag = mFlags.get(i);
            flag.set(false);
        }

        if (mBooleanServiceCreated && isConnected) {
            mServiceManager.disconnect();
            Connected_device = null;
            Connected_MacAddr = "";
            spinner_devselector.setEnabled(true);
            isConnected = !isConnected;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();


        System.exit(0);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ModelObject object = mScanListAdapter.getData().get(position);
        mac = object.getAddress();
        spinner_devselector.setSelection(findKoalaWithMac(mac));

        new AlertDialog.Builder(MainActivity.this, android.R.style.Theme_Material_Light_Dialog_Alert)
                .setTitle("Check ?")
                .setMessage("Connect to " + mac + " ?")
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        progressDialog = ProgressDialog.show(MainActivity.this, "Connecting", "Please wait ...", true);
                        startRun();
                        DismissDialogListView();

                    }
                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .show();
    }

    private void startRun() {
        if (spinner_devselector.getCount() >= 1) {
            mac = spinner_devselector.getSelectedItem().toString();

            Connected_device = mDevices.get(findKoalaWithMac(mac));
            Connected_device.resetSamplingRate();
            Connected_device.setConnectedTime();
            Connected_MacAddr = mac;

            if (mBooleanServiceCreated) {
                if (!isConnected) {
                    mServiceManager.connect(mac);
                    Toast.makeText(getBaseContext(), "Connection established to " + mac, Toast.LENGTH_SHORT).show();
                    //Update MAC

                    spinner_devselector.setEnabled(false);
                    isConnected = !isConnected;
                } else {
                    mServiceManager.disconnect();
                    Toast.makeText(getBaseContext(), "Disconnected from " + mac, Toast.LENGTH_SHORT).show();
                    Connected_device = null;
                    Connected_MacAddr = "";

                    spinner_devselector.setEnabled(true);
                    isConnected = !isConnected;
                }

            } else {
                Toast.makeText(getBaseContext(), "Failed to connect to : " + mac, Toast.LENGTH_SHORT).show();
                spinner_devselector.setEnabled(true);
                isConnected = false;
            }
        } else {
            Toast.makeText(getBaseContext(), "Scanner returned nothing", Toast.LENGTH_SHORT).show();
            spinner_devselector.setEnabled(true);
            isConnected = false;
        }
    }

    // ----------------------------------------------------------------------------- Override events

    /*
     * time smoothing constant for low-pass filter
     * 0 ≤ alpha ≤ 1 ; a smaller value basically means more smoothing
     * See: http://en.wikipedia.org/wiki/Low-pass_filter#Discrete-time_realization
     * http://blog.thomnichols.org/2011/08/smoothing-sensor-data-with-a-low-pass-filter
     */
    static final float ALPHA = 0.35f;
    protected float[] lowPass( float[] input, float[] output ) {
        if ( output == null ) return input;

        for ( int i=0; i<input.length; i++ ) {
            output[i] = output[i] + ALPHA * (input[i] - output[i]);
        }
        return output;
    }

}
