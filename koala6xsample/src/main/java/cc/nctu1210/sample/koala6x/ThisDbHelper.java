package cc.nctu1210.sample.koala6x;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Switch;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by nyotie on 11-Mar-16.
 */
public class ThisDbHelper extends SQLiteOpenHelper {

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 15;

    public static final String DATABASE_NAME = "koalaRssi.db";
    public static final String TABLE_NAMERAW = "KoalaRaw";
    public static final String TABLE_NAMEFEAT = "KoalaFeature";
    public static final String COLUMN_MAC = "MacAddress";
    public static final String COLUMN_RSSI = "rssi";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_accX = "accX";
    public static final String COLUMN_accY = "accY";
    public static final String COLUMN_accZ = "accZ";
    public static final String COLUMN_gyroX = "gyroX";
    public static final String COLUMN_gyroY = "gyroY";
    public static final String COLUMN_gyroZ = "gyroZ";
    public static final String COLUMN_magX = "magX";
    public static final String COLUMN_magY = "magY";
    public static final String COLUMN_magZ = "magZ";
    public static final String COLUMN_Quat0 = "quat0";
    public static final String COLUMN_Quat1 = "quat1";
    public static final String COLUMN_Quat2 = "quat2";
    public static final String COLUMN_Quat3 = "quat3";
    public static final String COLUMN_veloX = "veloX";
    public static final String COLUMN_veloY = "veloY";
    public static final String COLUMN_veloZ = "veloZ";

    public static final String COLUMN_flag = "flag";
    public static final String COLUMN_DATE = "recdate";

    public String databasePath = "";

    public ThisDbHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        databasePath = context.getDatabasePath(DATABASE_NAME).getPath();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + TABLE_NAMERAW + " (" +
                COLUMN_MAC + " string, " + COLUMN_RSSI + " float, " + COLUMN_ID + " integer primary key, " +
                COLUMN_accX + " float, " + COLUMN_accY + " float, " + COLUMN_accZ + " float, " +
                COLUMN_gyroX + " float, " + COLUMN_gyroY + " float, " + COLUMN_gyroZ + " float, " +
                COLUMN_magX + " float, " + COLUMN_magY + " float, " + COLUMN_magZ + " float, " +
                COLUMN_Quat0 + " float, " + COLUMN_Quat1 + " float, " + COLUMN_Quat2 + " float, " + COLUMN_Quat3 + " float, " +
                COLUMN_veloX + " float, " + COLUMN_veloY + " float, " + COLUMN_veloZ + " float, " +
                COLUMN_flag + " string, " + COLUMN_DATE + " text);");

        db.execSQL("create table " + TABLE_NAMEFEAT + " (" +
                COLUMN_MAC + " string, " + COLUMN_ID + " integer primary key, " +
                "colMeanAx float, colMeanAy float, colMeanAz float, colMeanGx float, colMeanGy float, colMeanGz float, " +
                "colVarAx float, colVarAy float, colVarAz float, colVarGx float, colVarGy float, colVarGz float, " +
                "colCovarAx float, colCovarAy float, colCovarAz float, colCovarGx float, colCovarGy float, colCovarGz float, " +
                "colCorelAx float, colCorelAy float, colCorelAz float, colCorelGx float, colCorelGy float, colCorelGz float, " +
                "colZCRateAx float, colZCRateAy float, colZCRateAz float, colZCRateGx float, colZCRateGy float, colZCRateGz float, " +
                "colSDevAx float, colSDevAy float, colSDevAz float, colSDevGx float, colSDevGy float, colSDevGz float, " +
                "colRMSqAx float, colRMSqAy float, colRMSqAz float, colRMSqGx float, colRMSqGy float, colRMSqGz float, " +
                "colSkewAx float, colSkewAy float, colSkewAz float, colSkewGx float, colSkewGy float, colSkewGz float, " +
                "colKurtAx float, colKurtAy float, colKurtAz float, colKurtGx float, colKurtGy float, colKurtGz float, " +
                "colMADevAx float, colMADevAy float, colMADevAz float, colMADevGx float, colMADevGy float, colMADevGz float, " +
                "colFFTMFreqAx float, colFFTMFreqAy float, colFFTMFreqAz float, colFFTMFreqGx float, colFFTMFreqGy float, colFFTMFreqGz float, " +
                "colFFTEgAx float, colFFTEgAy float, colFFTEgAz float, colFFTEgGx float, colFFTEgGy float, colFFTEgGz float, " +
                "colFFTEtAx float, colFFTEtAy float, colFFTEtAz float, colFFTEtGx float, colFFTEtGy float, colFFTEtGz float, " +
                "colMCRAx float, colMCRAy float, colMCRAz float, colMCRGx float, colMCRGy float, colMCRGz float, " +
                "colAADAx float, colAADAy float, colAADAz float, colAADGx float, colAADGy float, colAADGz float, " +
                "colAAVAx float, colAAVAy float, colAAVAz float, colAAVGx float, colAAVGy float, colAAVGz float, " +
                "colARA float, " +
                "colQuat1 float, colQuat2 float, colQuat3 float, colQuat4 float, " +
                COLUMN_DATE + " text, activityFlag string);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAMERAW + ";");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAMEFEAT + ";");
        onCreate(db);
    }


    public String getDatabaseName(){
        return this.DATABASE_NAME;
    }

    public String getDatabasePath(){
        return this.databasePath;
    }

    public boolean insertNewRowRaw(String mac, float rssi, float[] Accelerometer, float[] Gyroscope, float[] Magnetometer, float[] Quaternion, float[] Velocity, String flag) {
        try{
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();

            contentValues.put(COLUMN_MAC, mac);
            contentValues.put(COLUMN_RSSI, rssi);

            contentValues.put(COLUMN_accX, Accelerometer[0]);
            contentValues.put(COLUMN_accY, Accelerometer[1]);
            contentValues.put(COLUMN_accZ, Accelerometer[2]);

            contentValues.put(COLUMN_gyroX, Gyroscope[0]);
            contentValues.put(COLUMN_gyroY, Gyroscope[1]);
            contentValues.put(COLUMN_gyroZ, Gyroscope[2]);

            contentValues.put(COLUMN_magX, Magnetometer[0]);
            contentValues.put(COLUMN_magY, Magnetometer[1]);
            contentValues.put(COLUMN_magZ, Magnetometer[2]);

            contentValues.put(COLUMN_Quat0, Quaternion[0]);
            contentValues.put(COLUMN_Quat1, Quaternion[1]);
            contentValues.put(COLUMN_Quat2, Quaternion[2]);
            contentValues.put(COLUMN_Quat3, Quaternion[3]);

            contentValues.put(COLUMN_veloX, Velocity[0]);
            contentValues.put(COLUMN_veloY, Velocity[1]);
            contentValues.put(COLUMN_veloZ, Velocity[2]);

            contentValues.put(COLUMN_flag, flag);

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
            Date date = new Date();
            contentValues.put(COLUMN_DATE, dateFormat.format(date).toString());

            db.insert(TABLE_NAMERAW, null, contentValues);
            return true;
        } catch (Exception sqlEx){
            Log.e("sqlite",sqlEx.toString());
            return false;
        }
    }

    public boolean insertNewRowFeature(String mac, Object []accelFeatures, Object []gyroFeatures, float[] Quaternion, String actFlag){
        try{
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();

            contentValues.put(COLUMN_MAC, mac);

            contentValues.put("colMeanAx", (float) accelFeatures[0]);
            contentValues.put("colMeanAy", (float) accelFeatures[1]);
            contentValues.put("colMeanAz", (float) accelFeatures[2]);
            contentValues.put("colMeanGx", (float) gyroFeatures[0]);
            contentValues.put("colMeanGy", (float) gyroFeatures[1]);
            contentValues.put("colMeanGz", (float) gyroFeatures[2]);

            contentValues.put("colMADevAx", (float) accelFeatures[3]);
            contentValues.put("colMADevAy", (float) accelFeatures[4]);
            contentValues.put("colMADevAz", (float) accelFeatures[5]);
            contentValues.put("colMADevGx", (float) gyroFeatures[3]);
            contentValues.put("colMADevGy", (float) gyroFeatures[4]);
            contentValues.put("colMADevGz", (float) gyroFeatures[5]);

            contentValues.put("colVarAx", (float) accelFeatures[6]);
            contentValues.put("colVarAy", (float) accelFeatures[7]);
            contentValues.put("colVarAz", (float) accelFeatures[8]);
            contentValues.put("colVarGx", (float) gyroFeatures[6]);
            contentValues.put("colVarGy", (float) gyroFeatures[7]);
            contentValues.put("colVarGz", (float) gyroFeatures[8]);

            contentValues.put("colSDevAx", (float) accelFeatures[9]);
            contentValues.put("colSDevAy", (float) accelFeatures[10]);
            contentValues.put("colSDevAz", (float) accelFeatures[11]);
            contentValues.put("colSDevGx", (float) gyroFeatures[9]);
            contentValues.put("colSDevGy", (float) gyroFeatures[10]);
            contentValues.put("colSDevGz", (float) gyroFeatures[11]);

            contentValues.put("colSkewAx", (float) accelFeatures[12]);
            contentValues.put("colSkewAy", (float) accelFeatures[13]);
            contentValues.put("colSkewAz", (float) accelFeatures[14]);
            contentValues.put("colSkewGx", (float) gyroFeatures[12]);
            contentValues.put("colSkewGy", (float) gyroFeatures[13]);
            contentValues.put("colSkewGz", (float) gyroFeatures[14]);

            contentValues.put("colKurtAx", (float) accelFeatures[15]);
            contentValues.put("colKurtAy", (float) accelFeatures[16]);
            contentValues.put("colKurtAz", (float) accelFeatures[17]);
            contentValues.put("colKurtGx", (float) gyroFeatures[15]);
            contentValues.put("colKurtGy", (float) gyroFeatures[16]);
            contentValues.put("colKurtGz", (float) gyroFeatures[17]);

            contentValues.put("colZCRateAx", (float) accelFeatures[18]);
            contentValues.put("colZCRateAy", (float) accelFeatures[19]);
            contentValues.put("colZCRateAz", (float) accelFeatures[20]);
            contentValues.put("colZCRateGx", (float) gyroFeatures[18]);
            contentValues.put("colZCRateGy", (float) gyroFeatures[19]);
            contentValues.put("colZCRateGz", (float) gyroFeatures[20]);

            contentValues.put("colMCRAx", (float) accelFeatures[21]);
            contentValues.put("colMCRAy", (float) accelFeatures[22]);
            contentValues.put("colMCRAz", (float) accelFeatures[23]);
            contentValues.put("colMCRGx", (float) gyroFeatures[21]);
            contentValues.put("colMCRGy", (float) gyroFeatures[22]);
            contentValues.put("colMCRGz", (float) gyroFeatures[23]);

            contentValues.put("colAADAx", (float) accelFeatures[24]);
            contentValues.put("colAADAy", (float) accelFeatures[25]);
            contentValues.put("colAADAz", (float) accelFeatures[26]);
            contentValues.put("colAADGx", (float) gyroFeatures[24]);
            contentValues.put("colAADGy", (float) gyroFeatures[25]);
            contentValues.put("colAADGz", (float) gyroFeatures[26]);

            contentValues.put("colAAVAx", (float) accelFeatures[27]);
            contentValues.put("colAAVAy", (float) accelFeatures[28]);
            contentValues.put("colAAVAz", (float) accelFeatures[29]);
            contentValues.put("colAAVGx", (float) gyroFeatures[27]);
            contentValues.put("colAAVGy", (float) gyroFeatures[28]);
            contentValues.put("colAAVGz", (float) gyroFeatures[29]);

            contentValues.put("colRMSqAx", (float) accelFeatures[30]);
            contentValues.put("colRMSqAy", (float) accelFeatures[31]);
            contentValues.put("colRMSqAz", (float) accelFeatures[32]);
            contentValues.put("colRMSqGx", (float) gyroFeatures[30]);
            contentValues.put("colRMSqGy", (float) gyroFeatures[31]);
            contentValues.put("colRMSqGz", (float) gyroFeatures[32]);

            contentValues.put("colCorelAx", (float) accelFeatures[33]);
            contentValues.put("colCorelAy", (float) accelFeatures[34]);
            contentValues.put("colCorelAz", (float) accelFeatures[35]);
            contentValues.put("colCorelGx", (float) gyroFeatures[33]);
            contentValues.put("colCorelGy", (float) gyroFeatures[34]);
            contentValues.put("colCorelGz", (float) gyroFeatures[35]);

            contentValues.put("colCovarAx", (float) accelFeatures[36]);
            contentValues.put("colCovarAy", (float) accelFeatures[37]);
            contentValues.put("colCovarAz", (float) accelFeatures[38]);
            contentValues.put("colCovarGx", (float) gyroFeatures[36]);
            contentValues.put("colCovarGy", (float) gyroFeatures[37]);
            contentValues.put("colCovarGz", (float) gyroFeatures[38]);

            contentValues.put("colARA", (float) gyroFeatures[39]);

            contentValues.put("colQuat1", Quaternion[0]);
            contentValues.put("colQuat2", Quaternion[1]);
            contentValues.put("colQuat3", Quaternion[2]);
            contentValues.put("colQuat4", Quaternion[3]);

            contentValues.put("colFFTMFreqAx", 0);
            contentValues.put("colFFTMFreqAy", 0);
            contentValues.put("colFFTMFreqAz", 0);
            contentValues.put("colFFTMFreqGx", 0);
            contentValues.put("colFFTMFreqGy", 0);
            contentValues.put("colFFTMFreqGz", 0);

            contentValues.put("colFFTEgAx", 0);
            contentValues.put("colFFTEgAy", 0);
            contentValues.put("colFFTEgAz", 0);
            contentValues.put("colFFTEgGx", 0);
            contentValues.put("colFFTEgGy", 0);
            contentValues.put("colFFTEgGz", 0);

            contentValues.put("colFFTEtAx", 0);
            contentValues.put("colFFTEtAy", 0);
            contentValues.put("colFFTEtAz", 0);
            contentValues.put("colFFTEtGx", 0);
            contentValues.put("colFFTEtGy", 0);
            contentValues.put("colFFTEtGz", 0);

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
            Date date = new Date();
            contentValues.put(COLUMN_DATE, dateFormat.format(date).toString());

            contentValues.put("activityFlag", actFlag);

            db.insert(TABLE_NAMEFEAT, null, contentValues);
//            Log.d("sqlite","success insert featured data");
            return true;
        } catch (Exception sqlEx){
            Log.e("sqlite", sqlEx.toString());
            return false;
        }
    }

    public boolean insertNewRowFeature(String mac, double[] dtMean, double[] dtVariance, double[] dtCovar, double[] dtCorel, double[] dtZCRate, double[] dtSDev, double[] dtRMS, double[] dtSkew, double[] dtKurt, double[] dtMADev, double[] dtFFTMFreq, double[] FFTEg, double[] FFTEt, String actFlag){
        try{
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();

            contentValues.put(COLUMN_MAC, mac);

            contentValues.put("colMeanAx", dtMean[0]);
            contentValues.put("colMeanAy", dtMean[1]);
            contentValues.put("colMeanAz", dtMean[2]);
            contentValues.put("colMeanGx", dtMean[3]);
            contentValues.put("colMeanGy", dtMean[4]);
            contentValues.put("colMeanGz", dtMean[5]);

            contentValues.put("colVarAx", dtVariance[0]);
            contentValues.put("colVarAy", dtVariance[1]);
            contentValues.put("colVarAz", dtVariance[2]);
            contentValues.put("colVarGx", dtVariance[3]);
            contentValues.put("colVarGy", dtVariance[4]);
            contentValues.put("colVarGz", dtVariance[5]);

            contentValues.put("colCovarAx", dtCovar[0]);
            contentValues.put("colCovarAy", dtCovar[1]);
            contentValues.put("colCovarAz", dtCovar[2]);
            contentValues.put("colCovarGx", dtCovar[3]);
            contentValues.put("colCovarGy", dtCovar[4]);
            contentValues.put("colCovarGz", dtCovar[5]);

            contentValues.put("colCorelAx", dtCorel[0]);
            contentValues.put("colCorelAy", dtCorel[1]);
            contentValues.put("colCorelAz", dtCorel[2]);
            contentValues.put("colCorelGx", dtCorel[3]);
            contentValues.put("colCorelGy", dtCorel[4]);
            contentValues.put("colCorelGz", dtCorel[5]);

            contentValues.put("colZCRateAx", dtZCRate[0]);
            contentValues.put("colZCRateAy", dtZCRate[1]);
            contentValues.put("colZCRateAz", dtZCRate[2]);
            contentValues.put("colZCRateGx", dtZCRate[3]);
            contentValues.put("colZCRateGy", dtZCRate[4]);
            contentValues.put("colZCRateGz", dtZCRate[5]);

            contentValues.put("colSDevAx", dtSDev[0]);
            contentValues.put("colSDevAy", dtSDev[1]);
            contentValues.put("colSDevAz", dtSDev[2]);
            contentValues.put("colSDevGx", dtSDev[3]);
            contentValues.put("colSDevGy", dtSDev[4]);
            contentValues.put("colSDevGz", dtSDev[5]);

            contentValues.put("colRMSqAx", dtRMS[0]);
            contentValues.put("colRMSqAy", dtRMS[1]);
            contentValues.put("colRMSqAz", dtRMS[2]);
            contentValues.put("colRMSqGx", dtRMS[3]);
            contentValues.put("colRMSqGy", dtRMS[4]);
            contentValues.put("colRMSqGz", dtRMS[5]);

            contentValues.put("colSkewAx", dtSkew[0]);
            contentValues.put("colSkewAy", dtSkew[1]);
            contentValues.put("colSkewAz", dtSkew[2]);
            contentValues.put("colSkewGx", dtSkew[3]);
            contentValues.put("colSkewGy", dtSkew[4]);
            contentValues.put("colSkewGz", dtSkew[5]);

            contentValues.put("colKurtAx", dtKurt[0]);
            contentValues.put("colKurtAy", dtKurt[1]);
            contentValues.put("colKurtAz", dtKurt[2]);
            contentValues.put("colKurtGx", dtKurt[3]);
            contentValues.put("colKurtGy", dtKurt[4]);
            contentValues.put("colKurtGz", dtKurt[5]);

            contentValues.put("colMADevAx", dtMADev[0]);
            contentValues.put("colMADevAy", dtMADev[1]);
            contentValues.put("colMADevAz", dtMADev[2]);
            contentValues.put("colMADevGx", dtMADev[3]);
            contentValues.put("colMADevGy", dtMADev[4]);
            contentValues.put("colMADevGz", dtMADev[5]);

            contentValues.put("colFFTMFreqAx", dtFFTMFreq[0]);
            contentValues.put("colFFTMFreqAy", dtFFTMFreq[1]);
            contentValues.put("colFFTMFreqAz", dtFFTMFreq[2]);
            contentValues.put("colFFTMFreqGx", dtFFTMFreq[3]);
            contentValues.put("colFFTMFreqGy", dtFFTMFreq[4]);
            contentValues.put("colFFTMFreqGz", dtFFTMFreq[5]);

            contentValues.put("colFFTEgAx", FFTEg[0]);
            contentValues.put("colFFTEgAy", FFTEg[1]);
            contentValues.put("colFFTEgAz", FFTEg[2]);
            contentValues.put("colFFTEgGx", FFTEg[3]);
            contentValues.put("colFFTEgGy", FFTEg[4]);
            contentValues.put("colFFTEgGz", FFTEg[5]);

            contentValues.put("colFFTEtAx", FFTEt[0]);
            contentValues.put("colFFTEtAy", FFTEt[1]);
            contentValues.put("colFFTEtAz", FFTEt[2]);
            contentValues.put("colFFTEtGx", FFTEt[3]);
            contentValues.put("colFFTEtGy", FFTEt[4]);
            contentValues.put("colFFTEtGz", FFTEt[5]);

            contentValues.put("colMCRAx", 0);
            contentValues.put("colMCRAy", 0);
            contentValues.put("colMCRAz", 0);
            contentValues.put("colMCRGx", 0);
            contentValues.put("colMCRGy", 0);
            contentValues.put("colMCRGz", 0);

            contentValues.put("colAADAx", 0);
            contentValues.put("colAADAy", 0);
            contentValues.put("colAADAz", 0);
            contentValues.put("colAADGx", 0);
            contentValues.put("colAADGy", 0);
            contentValues.put("colAADGz", 0);

            contentValues.put("colAAVAx", 0);
            contentValues.put("colAAVAy", 0);
            contentValues.put("colAAVAz", 0);
            contentValues.put("colAAVGx", 0);
            contentValues.put("colAAVGy", 0);
            contentValues.put("colAAVGz", 0);

            contentValues.put("colARA", 0);

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
            Date date = new Date();
            contentValues.put(COLUMN_DATE, dateFormat.format(date).toString());

            contentValues.put("activityFlag", actFlag);

            db.insert(TABLE_NAMEFEAT, null, contentValues);
            return true;
        } catch (Exception sqlEx){
            Log.e("sqlite",sqlEx.toString());
            return false;
        }
    }

    /*
    public boolean updateRec(Integer id, double xValue, double yValue, double zValue) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_X, xValue);
        contentValues.put(COLUMN_Y, yValue);
        contentValues.put(COLUMN_Z, zValue);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        contentValues.put(COLUMN_DATE, dateFormat.format(date));
        db.update(TABLE_NAMERAW, contentValues, COLUMN_ID + " = ? ", new String[]{Integer.toString(id)});
        return true;
    }

    public Integer deleteRec(Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_NAMERAW,
                COLUMN_ID + " = ? ",
                new String[] { Integer.toString(id) });
    }
    */

    public void clearTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAMERAW + ";");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAMEFEAT + ";");
        onCreate(db);
    }

    public Cursor getAllRawData(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery(String.format("SELECT %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s FROM %s;",
                COLUMN_MAC, COLUMN_RSSI,
                COLUMN_accX, COLUMN_accY, COLUMN_accZ,
                COLUMN_gyroX, COLUMN_gyroY, COLUMN_gyroZ,
                COLUMN_Quat0, COLUMN_Quat1, COLUMN_Quat2, COLUMN_Quat3,
                COLUMN_veloX, COLUMN_veloY, COLUMN_veloZ,
                COLUMN_flag, COLUMN_DATE, TABLE_NAMERAW), null );
        return res;
    }

    public Cursor getAllFeatData(){
        SQLiteDatabase db = this.getReadableDatabase();
//        Cursor res =  db.rawQuery("SELECT recdate, colMeanAx, colMeanAy, colMeanAz, colMeanGx, colMeanGy, colMeanGz, "+
//               "colVarAx, colVarAy, colVarAz, colVarGx, colVarGy, colVarGz, "+
//               "colCovarAx, colCovarAy, colCovarAz, colCovarGx, colCovarGy, colCovarGz, "+
//               "colCorelAx, colCorelAy, colCorelAz, colCorelGx, colCorelGy, colCorelGz, "+
//               "colZCRateAx, colZCRateAy, colZCRateAz, colZCRateGx, colZCRateGy, colZCRateGz, "+
//               "colSDevAx, colSDevAy, colSDevAz, colSDevGx, colSDevGy, colSDevGz, "+
//               "colRMSqAx, colRMSqAy, colRMSqAz, colRMSqGx, colRMSqGy, colRMSqGz, "+
//               "colSkewAx, colSkewAy, colSkewAz, colSkewGx, colSkewGy, colSkewGz, "+
//               "colKurtAx, colKurtAy, colKurtAz, colKurtGx, colKurtGy, colKurtGz, "+
//               "colMADevAx, colMADevAy, colMADevAz, colMADevGx, colMADevGy, colMADevGz, "+
//               "colFFTMFreqAx, colFFTMFreqAy, colFFTMFreqAz, colFFTMFreqGx, colFFTMFreqGy, colFFTMFreqGz, "+
//               "colFFTEgAx, colFFTEgAy, colFFTEgAz, colFFTEgGx, colFFTEgGy, colFFTEgGz, "+
//               "colFFTEtAx, colFFTEtAy, colFFTEtAz, colFFTEtGx, colFFTEtGy, colFFTEtGz, activityFlag FROM " + TABLE_NAMEFEAT + ";", null );

//        Cursor res =  db.rawQuery("SELECT recdate, colMeanAx, colMeanAy, colMeanAz, colMeanGx, colMeanGy, colMeanGz, "+
//                "colVarAx, colVarAy, colVarAz, colVarGx, colVarGy, colVarGz, "+
//                "colCovarAx, colCovarAy, colCovarAz, colCovarGx, colCovarGy, colCovarGz, "+
//                "colCorelAx, colCorelAy, colCorelAz, colCorelGx, colCorelGy, colCorelGz, "+
//                "colSDevAx, colSDevAy, colSDevAz, colSDevGx, colSDevGy, colSDevGz, "+
//                "colRMSqAx, colRMSqAy, colRMSqAz, colRMSqGx, colRMSqGy, colRMSqGz, "+
//                "colSkewAx, colSkewAy, colSkewAz, colSkewGx, colSkewGy, colSkewGz, "+
//                "colKurtAx, colKurtAy, colKurtAz, colKurtGx, colKurtGy, colKurtGz, "+
//                "colMADevAx, colMADevAy, colMADevAz, colMADevGx, colMADevGy, colMADevGz, "+
//                "colFFTEgAx, colFFTEgAy, colFFTEgAz, colFFTEgGx, colFFTEgGy, colFFTEgGz, activityFlag FROM " + TABLE_NAMEFEAT + ";", null );

        Cursor res =  db.rawQuery("SELECT recdate, colMeanAx, colMeanAy, colMeanAz, colMeanGx, colMeanGy, colMeanGz, "+
                "colVarAx, colVarAy, colVarAz, colVarGx, colVarGy, colVarGz, "+
                "colCovarAx, colCovarAy, colCovarAz, colCovarGx, colCovarGy, colCovarGz, "+
                "colCorelAx, colCorelAy, colCorelAz, colCorelGx, colCorelGy, colCorelGz, "+
                "colZCRateAx, colZCRateAy, colZCRateAz, colZCRateGx, colZCRateGy, colZCRateGz, "+
                "colSDevAx, colSDevAy, colSDevAz, colSDevGx, colSDevGy, colSDevGz, "+
                "colRMSqAx, colRMSqAy, colRMSqAz, colRMSqGx, colRMSqGy, colRMSqGz, "+
                "colSkewAx, colSkewAy, colSkewAz, colSkewGx, colSkewGy, colSkewGz, "+
                "colKurtAx, colKurtAy, colKurtAz, colKurtGx, colKurtGy, colKurtGz, "+
                "colMADevAx, colMADevAy, colMADevAz, colMADevGx, colMADevGy, colMADevGz, "+
                "colMCRAx, colMCRAy, colMCRAz, colMCRGx, colMCRGy, colMCRGz, " +
                "colAADAx, colAADAy, colAADAz, colAADGx, colAADGy, colAADGz, " +
                "colAAVAx, colAAVAy, colAAVAz, colAAVGx, colAAVGy, colAAVGz, " +
                "colARA, colQuat1, colQuat2, colQuat3, colQuat4, activityFlag FROM " + TABLE_NAMEFEAT + ";", null );
        return res;
    }

    public Cursor getDataFromID(int id){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "SELECT * FROM " + TABLE_NAMERAW + " where " + COLUMN_ID + " = " + id + ";", null );
        return res;
    }

    public int numberOfRawDataRows(){
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor res =  db.rawQuery(String.format("SELECT * FROM %s;", TABLE_NAMERAW), null );
        int numRows = res.getCount(); //(int) DatabaseUtils.queryNumEntries(db, TABLE_NAMERAW);
        res.close();
        return numRows;
    }

    public int numberOfFeaturedDataRows(){
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor res =  db.rawQuery(String.format("SELECT * FROM %s;", TABLE_NAMEFEAT), null );
        int numRows = res.getCount(); //(int) DatabaseUtils.queryNumEntries(db, TABLE_NAMEFEAT);
        res.close();
        return numRows;
    }
}
